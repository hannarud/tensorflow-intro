---
title: "TensorFlow: Introduction & Principles"
author: "Hanna Rudakouskaya, Data Analyst @ Teqniksoft"
date: "February 1, 2017"
output: ioslides_presentation
# output: beamer_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## TensorFlow main Web-checkpoints

<center><img src="tensorflow_main.png" alt="TensorFlow logo" style="width: 480px;"/></center>

* project website: <https://www.tensorflow.org/>;
* TensorFlow opensourced (Nov 2015): <https://research.googleblog.com/2015/11/tensorflow-googles-latest-machine_9.html>;

## Evolution

<center><b>Neural Networks</b></center>
<br>
<center>then</center>
<br>
<center><b>Deep Learning</b></center>
<br>
<center>then</center>
<br>
<center><b>TensorFlow</b></center>

## Concept

> Relation is not a result!

* Lazy evaluation;
* First define operations within TensorFlow graph and then run the defined operations within sessions;

> **Graph** is like a scheme and **session** is like a construction place.

* TensorFlow can only work with the things that were passed explicitly.

## Installation

Runs on Linux, Mac and Windows, CPU or GPU mode

## Example: conda, Python 2.7

### Create a conda environment called tensorflow:

`$ conda create -n tensorflow python=2.7`

`$ source activate tensorflow # Your prompt will change`

### Install TensorFlow

`(tensorflow)$ conda install -c conda-forge tensorflow`

### ... Perform actions ...

### Deactivate the environment

`(tensorflow)$ source deactivate # Your prompt should change back`

## Example: training first model

Starting from the root of your source tree (for me its conda and tensorflow environment, `~/anaconda/envs/tensorflow/lib/python2.7/site-packages/tensorflow`), run:

<center><img src="run_convolutional.png" alt="Example on MNIST dataset by Google" height="514" width="741"></center>

## Basic workflow

Following https://habrahabr.ru/post/305578/.

See `Hello, TensorFlow!.ipynb`

## "Advanced: Titanic Kaggle dataset"

See https://github.com/hannarud/kaggle-titanic-tensorflow

## Useful links

https://www.tensorflow.org/get_started/basic_usage
https://www.tensorflow.org/resources/dims_types
https://www.tensorflow.org/tutorials/mnist/tf/
https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/mnist/fully_connected_feed.py
https://github.com/martinwicke/tensorflow-tutorial
https://github.com/nivwusquorum/tensorflow-deepq
